var app = angular.module('app',['ngRoute','app.mainController','app.loginController',
	'app.aboutController','app.directives','app.popController','app.configration','app.runung','app.ui.bootstrap.demo']);
app.config(function($routeProvider) {
	$routeProvider
	.when('/',{
      templateUrl : 'angular/pages/home.html',
      controller  : 'mainController'
	})
	.when('/about',{
		templateUrl : 'angular/pages/about.html',
		controller  : 'aboutController'

	})
	.when('/popexample',{
		templateUrl : 'angular/pages/popExample.html',
		controller  : 'popController'
	})
});