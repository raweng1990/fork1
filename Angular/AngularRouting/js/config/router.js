//create the module name sample
var sample = angular.module('sample',['ngRoute'])
sample.config(function($routeProvider){
	$routeProvider
	.when('/',{
		templateUrl : "templates/home.html",
		controller  : "mainController"
	})
	.when('/validation',{
		templateUrl:"templates/validation.html",
		controller :"mainController"
	})
})